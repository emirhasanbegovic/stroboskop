# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
mkdir dn01
cd dn01
git clone https://emirhasanbegovic@bitbucket.org/emirhasanbegovic/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/emirhasanbegovic/stroboskop/commits/4b64186ed9aef6122e6efb5772838eb74dda38e9

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/emirhasanbegovic/stroboskop/commits/dbcf0e21a9180fd2a09ac07f9b8285b9dc9ca6bd

Naloga 6.3.2:
https://bitbucket.org/emirhasanbegovic/stroboskop/commits/d01488a4acd5d6b60cb5bcc70a09519fc573621e

Naloga 6.3.3:
https://bitbucket.org/emirhasanbegovic/stroboskop/commits/32881a32e9e1df98d5f0209004e0a9b14e55a32b

Naloga 6.3.4:
https://bitbucket.org/emirhasanbegovic/stroboskop/commits/e3888ebae5c4bd9096b8b7df4b93c9842b16518f

Naloga 6.3.5:

```
git checkout master
git merge izgled
git commit -m "Združitev veje izgled in master"
git push
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/emirhasanbegovic/stroboskop/commits/b08c6d618da3d2bd8c20cef3fa60b00e34325be8?at=dinamika

Naloga 6.4.2:
https://bitbucket.org/emirhasanbegovic/stroboskop/commits/630c2aac104eb6e3c9b3286e30472157ac2f133a?at=dinamika

Naloga 6.4.3:
https://bitbucket.org/emirhasanbegovic/stroboskop/commits/ec51526e7a36e5ca2f74fa17cc98609d8e41bdd2?at=dinamika

Naloga 6.4.4:
https://bitbucket.org/emirhasanbegovic/stroboskop/commits/ebcefdd3e9e050801d8bfc67bc32bf065f62b904?at=dinamika